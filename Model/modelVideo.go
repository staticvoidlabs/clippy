package model

// Video struct defines the data model to hold a video object.
type Video struct {
	ChannelTitle  string
	Title         string
	OriginalTitle string
	PublishedAt   string
	Duration      string
	FilePath      string
	FileName      string
	ThumbURL      string
	ThumbFilepath string
}
