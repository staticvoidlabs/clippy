package main

import (
	"fmt"
	"time"

	"github.com/atotto/clipboard"

	Manager "./manager"

	_ "image/jpeg"
	_ "image/png"
)

func main() {

	// Member definition.
	mShouldExit := false
	mLastString := ""

	// Init config.
	currentConfig := Manager.GetCurrentConfig()
	fmt.Println("Clippy started and is waiting for jobs...")

	// Run Youtv post-processing if enabled.
	currentConfig.YoutvPostProcessingEnabled = true
	if currentConfig.YoutvPostProcessingEnabled {
		Manager.RunYoutvPostProcessing(currentConfig)
	}

	// Enter the main loop.
	for !mShouldExit {

		tmpClipboardContent, err := clipboard.ReadAll()

		if err != nil && err.Error() != "Der Vorgang wurde erfolgreich beendet." {
			fmt.Println(err)
		}

		// Check clipboard content.
		if mLastString != tmpClipboardContent {

			mLastString = tmpClipboardContent
			fmt.Print("   New clipboard entry: " + tmpClipboardContent)

			// Get first 23 chars of current clipboard content.
			tmpRunes := []rune(tmpClipboardContent)
			tmpSubstringStart := string(tmpRunes[0:23])

			if tmpSubstringStart != "https://www.youtube.com" {
				fmt.Print(" ...skipping\r\n")
				continue
			}

			// Reset clipboard.
			//clipboard.WriteAll("")

			go Manager.DownloadVideo(tmpClipboardContent, currentConfig)

			/*
				// Get videoID from URL.
				tmpStringArray := strings.Split(tmpClipboardContent, "?v=")
				tmpVideoID := tmpStringArray[1]

				// Get details for current video.
				tmpCurrentVideo := Manager.GetVideosByID(tmpVideoID)
				fmt.Print(" > (" + tmpCurrentVideo.ChannelTitle + ": " + tmpCurrentVideo.Title[:10] + "...) > downloading...")

				// Run pre-processing for video to be downloaded.
				tmpCurrentVideo = Manager.PreProcessVideoFile(tmpCurrentVideo, currentConfig)

				// Build download command string.
				cmdArg1 := tmpClipboardContent
				cmdArg2 := currentConfig.ArgOutput
				cmdArg3 := tmpCurrentVideo.FilePath + tmpCurrentVideo.FileName + currentConfig.ArgTemplateName
				cmd := exec.Command(currentConfig.FullPathYtDl, cmdArg1, cmdArg2, cmdArg3)

				// Show toast (download started).
				Manager.ShowToastOnDownloadStarted(tmpCurrentVideo)

				// Execute command and print output.
				out, err := cmd.CombinedOutput()

				if err != nil {
					fmt.Println("cmd.Run() failed with %s\n", err)
				}

				if currentConfig.YtDlLoggingEnabled {
					Manager.SaveYoutubeDLOutputToFile(string(out))
				}

				// Run post-processing for downloaded video.
				tmpCurrentVideo = Manager.PostProcessVideoFile(tmpCurrentVideo)

				// Show toast (download finished).
				Manager.ShowToastOnDownloadFinished(tmpCurrentVideo)

				fmt.Println(" completed.")
			*/

		}

		time.Sleep(1 * time.Second)

	}

}
