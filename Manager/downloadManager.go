package manager

import (
	"fmt"
	"os/exec"
	"strings"

	Models "../model"
)

func DownloadVideo(clipboardContent string, currentConfig Models.Config) {

	// Get videoID from URL.
	tmpStringArray := strings.Split(clipboardContent, "?v=")
	tmpVideoID := tmpStringArray[1]

	// Get details for current video.
	tmpCurrentVideo := GetVideosByID(tmpVideoID)
	fmt.Print(" > (" + tmpCurrentVideo.ChannelTitle + ": " + tmpCurrentVideo.Title[:10] + "...) > downloading...")

	// Run pre-processing for video to be downloaded.
	tmpCurrentVideo = PreProcessVideoFile(tmpCurrentVideo, currentConfig)

	// Build download command string.
	cmdArg1 := clipboardContent
	cmdArg2 := currentConfig.ArgOutput
	cmdArg3 := tmpCurrentVideo.FilePath + tmpCurrentVideo.FileName + currentConfig.ArgTemplateName
	cmd := exec.Command(currentConfig.FullPathYtDl, cmdArg1, cmdArg2, cmdArg3)

	// Show toast (download started).
	ShowToastOnDownloadStarted(tmpCurrentVideo)

	// Execute command and print output.
	out, err := cmd.CombinedOutput()

	if err != nil {
		fmt.Println("cmd.Run() failed with %s\n", err)
	}

	if currentConfig.YtDlLoggingEnabled {
		SaveYoutubeDLOutputToFile(string(out))
	}

	// Run post-processing for downloaded video.
	tmpCurrentVideo = PostProcessVideoFile(tmpCurrentVideo)

	// Show toast (download finished).
	ShowToastOnDownloadFinished(tmpCurrentVideo)

	fmt.Println(" completed.")

}
