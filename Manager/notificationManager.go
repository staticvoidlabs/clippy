package manager

import (
	"fmt"

	Models "../model"
	"github.com/go-toast/toast"
)

// ShowToastOnDownloadStarted created a toast notification indicating an started download.
func ShowToastOnDownloadStarted(currentVideo Models.Video) {

	notification := toast.Notification{
		AppID:   "Clippy",
		Title:   "Clippy: Download gestartet",
		Message: currentVideo.ChannelTitle + ": " + currentVideo.Title,
		//Icon:    currentVideo.ThumbFilepath,
	}

	err := notification.Push()
	if err != nil {
		fmt.Println(err)
	}

}

// ShowToastOnDownloadFinished created a toast notification indicating an finished download.
func ShowToastOnDownloadFinished(currentVideo Models.Video) {

	notification := toast.Notification{
		AppID:   "Clippy",
		Title:   "Clippy: Download abgeschlossen",
		Message: currentVideo.ChannelTitle + ": " + currentVideo.Title,
		Icon:    currentVideo.ThumbFilepath,
	}

	err := notification.Push()
	if err != nil {
		fmt.Println(err)
	}

}
