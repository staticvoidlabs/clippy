package manager

import (
	"bytes"
	"fmt"
	"image"
	"image/png"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
)

func GenerateThumbPNG(url string, videoID string) string {
	// Create the file
	out, err := os.Create("./cache/" + videoID + ".jpg")
	if err != nil {
		fmt.Println(err)
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		fmt.Println(err)
	}

	imgByte, _ := ioutil.ReadFile("./cache/" + videoID + ".jpg")

	img, _, errDecode := image.Decode(bytes.NewReader(imgByte))
	if errDecode != nil {
		fmt.Println(errDecode)
	}

	outFilePNG, _ := os.Create("./cache/" + videoID + ".png")
	_ = png.Encode(outFilePNG, img)

	tmpabsolutePath, err := filepath.Abs("./cache/" + videoID + ".jpg")

	return tmpabsolutePath
}

func SaveYoutubeDLOutputToFile(output string) {

}
